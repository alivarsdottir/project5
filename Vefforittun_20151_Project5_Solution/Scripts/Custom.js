﻿$(document).ready(function () {

    $(function () {
        $('#reviewform').on('submit', function () {
            //alert('test');
            var form = $(this);
            $.ajax({
                url: '/MovieApp/ReviewMovie',
                data: form.serialize(),
                method: 'POST',
                success: function (responseData) {
 

                    $('#reviews').html('<h4>Reviews</h4>');
                    for (var i = 0; i < responseData.length; i++) {

                        var html = '<p>' + responseData[i].Username + ' | ' + responseData[i].Text + '</p>'

                        $('#reviews').append(html);

                    }
                }

            })

            return false;

        });
    });

    $(function () {

        var html = "<div class=" + '"starRate">';
        html += "<div>Currently rated: 3 stars<b></b></div>";
        html += "<ul>";
        html += "<li><a href=" + '"#"' + "><span>Give it 10 stars</span></a></li>";
        html += "<li><a href=" + '"#"' + "><span>Give it 9 stars</span></a></li>";
        html += "<li><a href=" + '"#"' + "><span>Give it 8 stars</span></a></li>";
        html += "<li><a href=" + '"#"' + "><span>Give it 7 stars</span></a></li>";
        html += "<li><a href=" + '"#"' + "><span>Give it 6 stars</span></a></li>";
        html += "<li><a href=" + '"#"' + "><span>Give it 5 stars</span></a></li>";
        html += "<li><a href=" + '"#"' + "><span>Give it 4 stars</span></a></li>";
        html += "<li><a href=" + '"#"' + "><span>Give it 3 stars</span><b></b></a></li>";
        html += "<li><a href=" + '"#"' + "><span>Give it 2 stars</span><b></b></a></li>";
        html += "<li><a href=" + '"#"' + "><span>Give it 1 star</span><b></b></a></li>";
        html += "</ul>";
        html += "</div>";

       $('#ratingform').replaceWith(html);
    });

    $(function () {
        $('#ratingform').on('submit', function () {

            var form = $(this);

            $.ajax({
                url: '/MovieApp/RateMovie',
                data: form.serialize(),
                method: 'POST',
                success: function (responseData) {

                    console.log(responseData);
                    for (var i = 0; i < 1; i++) {

                        var overallRating = '<h3 id="ratingOverall">Rating: ' + responseData.overallrating + '</h3>';

                        var htmlif = '<p id = "if"> I rated this ' + responseData.currentrating.rating + " Stars" + '</p>';
                        var htmlelse = '<p id = "else">You have not rated this movie yet</p>';

                        if (responseData.currentrating != null)
                        {
                            $('#if').html(htmlif);
                        }
                        else
                        {
                            $('#else').html(htmlelse);
                        }

                        console.log(responseData);
                        $('#if').replaceWith(htmlif);
                        $('#else').replaceWith(htmlelse);


                        $('#ratingOverall').replaceWith(overallRating);

                    }



                }
            })
            //e.preventDefault();
            return false;

        });
    });
});